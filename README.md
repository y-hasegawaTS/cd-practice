# OpenShift GitOps (ArgoCD) による CD ハンズオン
[OpenShift GitOps](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/cicd/understanding-openshift-gitops) を用いてアプリケーションの継続的デリバリー (Continuous Delivery) を実施します。

OpenShift では 開発や本番環境を Project (Namespace) を利用することで、アクセスを許可させるユーザ、システムリソースのクォータ、デプロイするアプリケーションを隔離させます。
このハンズオンでは開発環境で CI パイプラインを用いてビルドしたアプリケーションを GitOps を用いて自動でステージング環境と本番環境にデプロイします。
* {USER}-develop: 開発環境を想定、この Project でコンテナイメージをビルドする
* {USER}-staging: ステージング環境を想定、主に結合テストでの利用を想定し、イメージのビルドは行わず、ビルドしたイメージをデプロイする
* {USER}-production: 本番環境を想定、ステージング環境と同様にイメージのビルドは行わず、ビルドしたイメージをデプロイする

アプリケーションの更新におけるリリース管理を想定し、Git リポジトリで管理するアプリケーションに変更を加え、これを契機に各環境に自動でアプリケーションを再デプロイします。
また加えた変更が機能的に不要または要望と異なるなどの変更に対する改修を想定し、最後に加えた変更を切り戻すため、ロールバックさせます。

## Table of Contents
- [OpenShift GitOps (ArgoCD) による CD ハンズオン](#openshift-gitops-argocd-による-cd-ハンズオン)
  - [Table of Contents](#table-of-contents)
  - [1. CI パイプラインの実行](#1-ci-パイプラインの実行)
    - [1.1. OpenShift Web Console へのログインと Web Terminal の起動](#11-openshift-web-console-へのログインと-web-terminal-の起動)
    - [1.2. アクセストークンの取得](#12-アクセストークンの取得)
    - [1.3. CI パイプラインの作成と実行](#13-ci-パイプラインの作成と実行)
  - [2. アプリケーションのデプロイ](#2-アプリケーションのデプロイ)
    - [2.1. ユーザインタフェースへのログイン](#21-ユーザインタフェースへのログイン)
    - [2.2. マニフェストリポジトリの接続](#22-マニフェストリポジトリの接続)
    - [2.3. 開発環境へのデプロイ](#23-開発環境へのデプロイ)
    - [2.4. リポジトリの同期確認](#24-リポジトリの同期確認)
    - [2.5. Git リポジトリへの変更](#25-git-リポジトリへの変更)
    - [2.6. ステージング環境へのデプロイ](#26-ステージング環境へのデプロイ)
    - [2.7. 本番環境へのデプロイ](#27-本番環境へのデプロイ)
  - [3. 開発環境のアプリケーション更新をステージング環境～本番環境に反映](#3-開発環境のアプリケーション更新をステージング環境本番環境に反映)
    - [3.1. アプリケーションソースコードの変更の適用](#31-アプリケーションソースコードの変更の適用)
    - [3.2. 開発環境へのアプリケーションの更新](#32-開発環境へのアプリケーションの更新)
    - [3.3. ステージング環境へのアプリケーションの更新](#33-ステージング環境へのアプリケーションの更新)
    - [3.4. 本番環境へのアプリケーションの更新](#34-本番環境へのアプリケーションの更新)
    - [3.5. アプリケーションのロールバック](#35-アプリケーションのロールバック)

---
## 1. CI パイプラインの実行
事前準備として、 開発環境でアプリケーションをビルドするための CI パイプラインを作成します。
新たに CI パイプラインを作成するため、[CI ハンズオン](https://gitlab.com/openshift-starter-kit/ci-practice)で作成した CI パイプラインを利用する必要はありません。

パイプラインの作成にあたり、アプリケーション本体とそのアプリケーションを展開するためのマニフェストを格納する 2 種類の Git リポジトリを利用します。
**これらの Git リポジトリは事前にクローンしています。**
* ソースコードを含むアプリケーションリポジトリー: https://gitlab.com/openshift-starter-kit/cicd-sample-app
CI パイプラインでビルドする Patient Health Records アプリケーションのソースコードを格納します。
* アプリケーションの必要な状態を定義する環境設定リポジトリ: https://gitlab.com/openshift-starter-kit/cd-practice
アプリケーションをデプロイさせる `Deployment` や Pod へのアクセスを振り分ける `Service` などのマニフェストを格納します。

### 1.1. OpenShift Web Console へのログインと Web Terminal の起動
OpenShift Web Console にログインし、規定の認証状を入力します。ログイン画面で Identity Provider の選択肢が複数表示される場合、htpasswd_providerを選択します。
ログイン後、以下の手順で Web Terminal を起動します。
![webterminal](./handson/img/web-terminal.png)

### 1.2. アクセストークンの取得
このハンズオンでは Git リポジトリに OpenShift クラスタにデプロイした [Gitea](https://gitea.io/en-us/) を利用します。Gitea はセルフホスト型 Git サービスです。
OpenShift Pipelines や OpenShift GitOps が Gitea の Git リポジトリを操作させるため、アクセストークンを生成します。

まず、環境変数にユーザ名と Gitea のホスト名を設定します。環境変数は、当ハンズオンのコマンド中や、ファイル中の文字列との置き換えなどで利用されます。
```
export USER=$(oc whoami)
export GITEA_HOSTNAME=$(oc get route gitea -n ${USER}-develop -ojsonpath='{.spec.host}')
```

以下のコマンド Gitea の URL を表示し、WEB ブラウザで Gitea Console にログインします。
認証情報は ID は `gitea`、パスワードは `openshift` となります。
```
echo http://${GITEA_HOSTNAME}
```

次に、Gitea のアクセストークンを作成します。
Gitea 画面右上の "Profile and Settings..." アイコンから "Settings" を選択します。
![Gitea Generate Token 01](./handson/img/gitea-generate-token-01.png)

"Application" タブを開き、"Token Name" に任意の名前 (e.g. gitea-token) を入力し、**"Generate Token"** ボタンを押します。
自動的にアクセストークンが作成され、画面上部に表示されます。

**トークンはこのページから移動すると二度と表示されないため、別途テキストなどに貼り付けます。トークンをなくした場合、もう一度同じ手順で再作成します。**

![Gitea Generate Token 02](./handson/img/gitea-generate-token-02.png)

作成したアクセストークンを環境変数に登録します。
```
export GITEA_TOKEN=<作成したトークン>
```

### 1.3. CI パイプラインの作成と実行
次のコマンドを実行し、CI パイプラインを作成し、実行します。
```
git clone http://${GITEA_HOSTNAME}/gitea/cd-practice.git
cd ./cd-practice/handson/
sh prerequisite.sh
```
Git リポジトリにプッシュする際に認証情報が求められるため、Username に `gitea`、Password に `openshift` を指定します。
```
[出力例]

Username for 'http://gitea-user11-develop.apps.cluster-xjbkv.xjbkv.sandbox180.opentlc.com':
Password for 'http://gitea@gitea-user11-develop.apps.cluster-xjbkv.xjbkv.sandbox180.opentlc.com':
```

OpenShift Web Console からパイプラインの実行に成功しているか確認します。
Web コンソールの左のメニューから **"Pipelines" > "Pipelines"** を選択し、画面上部の Project を選択する場所で **"{USER}-develop"** を選択します。
右側の画面で **"PipelineRuns"** タブをクリック、"PLR" というマークが付いたエントリーが見つかります。
これの Status が **"Succeeded"** であれば成功です。処理が継続している場合、数分程度処理が完了するまで待ちます。
![PipelineRun Success 01](./handson/img/pipelinerun-success-01.PNG)

エントリーの名前をクリックすると、実行されたパイプラインの中身やログを見ることができます。
![PipelineRun Success 02](./handson/img/pipelinerun-success-02.PNG)

---
## 2. アプリケーションのデプロイ
OpenShift GitOps を用いて次の環境にアプリケーションをデプロイします。
* 開発環境 : {USER}-develop
* ステージング環境 : {USER}-staging
* 本番環境 : {USER}-production

### 2.1. ユーザインタフェースへのログイン
OpenShift GitOps は専用のダッシュボードを提供し、OpenShift の認証情報でログインすることができます。
まず、ユーザインタフェースにログインします。

OpenShift Web コンソールにログインすると、画面上部に小さな四角が3x3で並んだアイコンが見つかります。
ここをクリックし **"Cluster Argo CD"** ボタンをクリックします。
![Argo CD UI Login 01](./handson/img/argocd-login-01.PNG)

別ウィンドウが開き、画面の右側の **"LOG IN VIA OPENSHIFT"** をクリックします。
![Argo CD UI Login 02](./handson/img/argocd-login-02.PNG)

OpenShift Web コンソールのログイン画面に移り、次のようなアクセス認証の画面が表示される場合、**user:Info** にチェックを入れたまま **"Allow selected permissions"** ボタンをクリックします。
![Argo CD UI Login 03](./handson/img/argocd-login-03.PNG)

現時点では OpenShift GitOps に関する設定を適用していないため、"No applications yet" とアプリケーションの情報は表示されません。
![Argo CD UI Login 04](./handson/img/argocd-login-04.PNG)

### 2.2. マニフェストリポジトリの接続
OpenShift Gitops にマニフェストを取得させるため、マニフェストを格納する Git リポジトリに接続させます。
左端にある歯車のアイコンをクリックし、"Settings" 画面に移り、一番上にある **"Repositories"** をクリックします。
![Argo CD Repo 01](./handson/img/argocd-repo-01.PNG)

**"CONNECT REPO USING HTTPS"** をクリックします。
![Argo CD Repo 02](./handson/img/argocd-repo-02.PNG)

次に接続させる Git リポジトリの情報を入力します。
以下の項目を入力して、**"CONNECT"** ボタンをクリックします。
* Type: git
* Project: (空白)
* Repository URL: マニフェストを格納する Git リポジトリの URL ※末尾に `.git` を必ず指定します。
Web Terminal から `echo http://${GITEA_HOSTNAME}/gitea/cd-practice.git` を実行し、出力結果を貼り付けます。
* Username: gitea
* Password: openshift
![Argo CD Repo 03](./handson/img/argocd-repo-03.PNG)

"CONNECTION STATUS" が "Successful" と表示されると接続に成功しています。
![Argo CD Repo 04](./handson/img/argocd-repo-04.PNG)

### 2.3. 開発環境へのデプロイ
開発環境にアプリケーションをデプロイします。OpenShift GitOps はアプリケーションのデプロイを管理するため `Application` と `AppProject` のカスタムリソースを必要とし、ここでは開発環境にアプリケーションをデプロイさせるためこのリソースを作成します。
* `Application`: アプリケーションの情報を定義するリソースで、マニフェストを格納する Git リポジトリの URL やアプリケーションをデプロイするクラスタや Project などを定義する
* `AppProject`: OpenShift の Project (Namespace) とは独立した OpenShift GitOps の概念で `Application` をグルーピングさせ `Application` を `AppProject` に関連付けるためのリソースで、マニフェストを格納する Git リポジトリの URL やアプリケーションをデプロイするクラスタや Project などを定義する
`Application` と `AppProject` の詳細は [Declarative Setup¶](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#applications) を参照下さい。

このハンズオンでは環境ごとに `Applicaiton` と `AppProject` を次のように関連付けます。
* "{USER}-app-develop" Application <--> "{USER}-dev" AppProject
* "{USER}-app-staging" Application <--> "{USER}-stg" AppProject
* "{USER}-app-production" Application <--> "{USER}-prod" AppProject

`oc apply -f` コマンドでマニフェストのパスを指定し、開発環境の `Application` と `AppProject` を作成します。
既に各マニフェストに定義は設定されているため、られているので、以下のコマンドを実行するだけ構いません。
設定内容を確認したい方は `cat` や `view` コマンドで定義を参照して下さい。
```
cd ~/cd-practice/handson/argocd
oc apply -f dev-project.yaml

[出力例]
appproject.argoproj.io/{USER}-dev created
```
```
oc apply -f dev-app.yaml

[出力例]
application.argoproj.io/{USER}-app-develop created
```

OpenShift GitOps のダッシュボードに戻ると、先ほどは何も表示されていなかった画面に "{USER}-app-develop" Application が表示され、Project に "{USER}-dev" と AppProject の名前が表示されます。
成功している場合、Appliation の左縁は緑色で Status は "Healthy" と表示されます。
![Argo CD Dev App 01](./handson/img/argocd-dev-app-01.PNG)

Application 全体をクリックすると、`Deployment`, `Service` や `Route` などデプロイされた OpenShift のリソースが表示されます。
![Argo CD Dev App 02](./handson/img/argocd-dev-app-02.PNG)

実際に OpenShift クラスタ側でもこれらのリソースを確認することができ、例えば、`Route` リソースを OpenShift Web Console から確認します。
OpenShift Web コンソールにログインし、左のメニューから **"Networking" > "Routes"** を選択し、画面上部の Project を選択する場所で **"{USER}-develop"** を選択します。
すると、右側の画面で "health-record" というエントリが見つかり、これが OpenShift GitOps によって作られた `Route` リソースです。
![Argo CD Dev App 03](./handson/img/argocd-dev-app-03.PNG)

"health-record" リソースの "Location" の列にある "http://" で始まる URL をクリックすると、別ウィンドウで "Demo Health" と表示された Web アプリケーションの画面が開きます。
これがリポジトリに格納されていたアプリケーションです。

"Login" の欄で、何でもよいので適当な名前を入力して "Sign In" ボタンを押すと、次のような画面が表示されます。
このアプリケーションはデモ用のアプリケーションなので、ユーザ認証は実装していません。
![Argo CD Dev App 04](./handson/img/argocd-dev-app-04.PNG)

以上で、OpenShift GitOps を使って開発環境にアプリケーションをデプロイすることができました。

### 2.4. リポジトリの同期確認
先ほど Application のステータスを確認した際、**"Synced"** というステータス表示があり、これはマニフェストリポジトリで宣言しているリソースと OpenShift クラスタでデプロイされているリソースの実態が同期していることを意味します。
![Argo CD Dev App 02](./handson/img/argocd-dev-app-02.PNG)

GitOps のコンセプトは Git リポジトリにマニフェストを管理し、変更は Git リポジトリに加えることであるため、OpenShift Web Console から意図的に `Deployment` などの設定に変更を加えると、OpenShift GitOps が変更を検出し、Git リポジトリの設定に戻します。
例えば Pod Replica 数は 上図の一番右側の列に表示され Pod は 1 つだけデプロイされています。
これを OpenShift クラスタから変更し、設定が切り戻されることを確認します。

OpenShift Web コンソールにログインし、左のメニューから **"Workloads" > "Deployments"** を選択し、画面上部の Project を選択する場所で **"{USER}-develop"** を選択します。
右側の画面で "health-record" エントリーの右側にある縦に点が3つ並んだアイコンをクリックし、いくつかメニューが表示され一番上にある **"Edit Pod count"** を選択します。
![Argo CD Sync 01](./handson/img/argocd-sync-01.PNG)

"Edit Pod count" というポップアップが出てきたら、これを適当な数に増やし、**"Save"** ボタンをクリックします。
![Argo CD Sync 02](./handson/img/argocd-sync-02.PNG)

上図では、Pod count を 5 に設定しているので、OpenShift の機能で Pod が5つにスケールアウトされるはずですが、何度操作しても増えることはなく、反対に、Pod count を 0 に減らしても、すぐに 1 に戻されます。
これは Git リポジトリに含むマニフェストで Pod Replica 数を 1 と宣言しているためです。

OpenShift GitOps は OpenShift クラスタ上に設定された `Deployment` や `Service` などリソースの設定を監視、マニフェストで宣言されている内容とクラスタに登録されたリソースの設定に差異があるとそれを検出します。
この差異は一般的にドリフトと呼ばれ、ドリフトが起きるとマニフェストの通りになるよう自動的に設定を同期して切り戻します。
これは Pod Replica 数だけではなく `Route` や `Service` など、マニフェストで宣言されているリソースは全て、ドリフトが発生すると修正されます。

この自動的に同期するポリシーを **"Auto-sync"** と呼びます。
Auto-sync の様子は、OpenShift Web コンソールと Argo CD の画面を横に並べて、先ほどのように Pod count を増やしてみるとわかりやすいです。
Pod count を変更すると、直ちに Argo CD の Application は **"OutOfSync"** のステータスに変化し、そして Auto-sync によって修正される様子が見られるでしょう。
![Argo CD Sync 03](./handson/img/argocd-sync-03.PNG)

### 2.5. Git リポジトリへの変更
今度は反対に Git リポジトリに格納されたマニフェストに対し Pod Replica 数を変更し、変更をプッシュ、自動で OpenShift クラスタに設定が反映されることを確認します。

Gitea の cd-practice リポジトリのソースコードの変更は、クローンしたローカルの cd-practice リポジトリを編集して `git push` する方法でもいいですし、Gitea の画面から直接変更する方法でも構いません。ここでは Gitea の画面から直接変更する方法でやってみます。

まず、Gitea の cd-practice リポジトリの画面に行きます。**master ブランチ** が選択された状態で、画面にディレクトリ/ファイル一覧が表示されています。

ハンズオンで Argo CD が参照しているマニフェストは、`./handson/deploy/` 以下にあり、Pod Replica 数を宣言しているマニフェストファイルは `./handson/deploy/base/health-deploy.yaml` です。
このディレクトリ/ファイルの一覧で、**"handson" > "deploy" > "base" > "health-deploy.yaml"** と順番に辿って、`health-deploy.yaml` ファイルの画面に移ります。
![Gitea CD Manifest modify 01](./handson/img/gitlab-cd-manifest-modify-01.PNG)

コードの右上に表示された **"Edit File"** ボタンをクリックし、ファイルを編集します。
`health-deploy.yaml` ファイルの 8 行目にある、`replicas: 1` を replicas: 2` と変更し **"Commit directly to the master branch."** のラジオボタンをチェックして、**"Commit Changes"** ボタンを押します。
コミットメッセージを入力しても構いませんし、しなくても構いません。
![Gitea CD Manifest modify 02](./handson/img/gitlab-cd-manifest-modify-02.PNG)

コミットしたら改めて `health-deploy.yaml` ファイルの中身を見て、変更が反映されていることを確認します。
OpenShift GitOps のダッシュボードに戻ると、5 分間程度で新しい Pod が追加され 2 つになります。
OpenShift クラスタでも、Pod が2つになっていることが確認できるでしょう。
![Argo CD Sync 04](./handson/img/argocd-sync-04.PNG)

このように、マニフェストに変更を加えると、クラスタの実態はマニフェストに合うように同期されます。

つまり、OpenShift クラスタを一切触ることなく、

**「マニフェストのみを変更することで、実態のアプリケーションに変更を加えていくことができる」**

これこそが、GitOps の本質的な部分です。

### 2.6. ステージング環境へのデプロイ
ステージング環境へアプリケーションをデプロイします。
開発環境と同じように、`oc apply -f` コマンドでマニフェストのパスを指定し、開発環境の `Application` と `AppProject` を作成します。
```
cd ~/cd-practice/handson/argocd
oc create -f stg-project.yaml

[出力例]
appproject.argoproj.io/{USER}-stg created
```
```
oc create -f stg-app.yaml

[出力例]
application.argoproj.io/{USER}-app-staging created
```

開発環境では、このコマンドを実行するだけで自動的にデプロイされましたが、ステージング環境では "{USER}-app-staging" Application は作られているものの、リソースはデプロイされていないでしょう。

これは、ステージング環境の "{GITLAB_USER}-app-staging" Application は **staging ブランチ** を参照させているためで、`stg-app.yaml` ファイルを参照すると [spec.source.TargetRevision](https://github.com/argoproj/argo-cd/blob/master/manifests/crds/application-crd.yaml#L366-L370) フィールドにタグやコミット ID などを指定することができ、ここでは `TargetRevision` フィールドに `staging` タグを指定しています。

作成されているブランチを確認すると、リポジトリには **master ブランチ** だけがあり、これは次のように "Branch: master" と表示されているドロップダウンメニューを開くことで確認できます。
![Argo CD Stg App 01](./handson/img/gitea-argocd-stg-app-01.png)

開発環境では Application が **master ブランチ** を参照させていたため、デプロイされましたが、ステージング環境では **staging ブランチ** を作成する必要があります。

ブランチを作成する方法は Gitea の画面でも、`git` コマンドでもどちらもありますが、ここでは Gitea 画面から作ります。
次のように "Branchs" タブから "master" ブランチの `Create new branch from 'master'` ボタンを押します。
![Argo CD Stg App 02](./handson/img/gitea-argocd-stg-app-02.png)

"Create new branch" の画面に移り、ここで "Branch name" に **"staging"** と入力し、**"Create branch"** ボタンをクリックします。
![Argo CD Stg App 03](./handson/img/gitea-argocd-stg-app-03.png)

リポジトリの画面に戻り、**staging ブランチ** が作られていることが確認でき、下図の画面で開いているメニューから **staging ブランチ** と **master ブランチ** を切り替えることができます。
切り替えると、両方のブランチが全く同じファイルを持っていることが確認できます。
![Argo CD Stg App 04](./handson/img/gitea-argocd-stg-app-04.png)

さて、ここで再び Argo CD の画面で "{USER}-app-staging" Application を見てみましょう。どうでしょうか ?
開発環境と全く同じように、リソースがデプロイされているはずです。デプロイされていない場合は、もう少し待ってみて下さい。

master ブランチと全く同じファイルを持つ staging ブランチが作成されたことで開発環境と全く同じようにステージング環境でもアプリケーションがデプロイされたことがわかります。
![Argo CD Stg App 05](./handson/img/argocd-stg-app-05.PNG)

最後に、開発環境でも行ったように、OpenShift Web コンソールで、"{USER}-staging" Project に作られた "health-record" Route の Location URL にアクセスして、"Demo Health" Web アプリケーションが稼働していることを確認してみて下さい。

### 2.7. 本番環境へのデプロイ
次は本番環境でのアプリケーションのデプロイを行います。
これは、前節のステージング環境でのアプリケーションのデプロイと、同じ手順で実施できます。
そのため本節は演習としたいと思います。詳細な手順は記載しませんが、ヒントを残しますので、挑戦してみて下さい。

本番環境でも次のようにアプリケーションがデプロイできたら OK です。
![Argo CD Prod App 01](./handson/img/argocd-prod-app-01.PNG)

**ヒント**

* 最初に実行するコマンド
```
cd ~/cd-practice/handson/argocd
oc create -f prod-project.yaml
oc create -f prod-app.yaml
```
* Gitea でのブランチ作成
作成するブランチの名前 : **production**
元となるブランチ : **staging**

---
## 3. 開発環境のアプリケーション更新をステージング環境～本番環境に反映
現時点では、開発、ステージング、本番環境で、全く同じアプリケーションが稼働していますが、実際のアプリケーション開発の現場ではこういう状況はほぼなく、開発もステージング環境も、本番環境よりもも新しいバージョンのアプリケーションが稼働していることが多いでしょう。
一般的にアプリケーションのリリースサイクルは 開発環境 -> ステージング環境 -> 本番環境の流れで、アプリケーションは初めに開発環境で更新され、様々なテストやチェックを経て、ステージング環境に更新が反映され、ステージング環境でも様々なテストやチェックを経て、最終的に本番環境に更新が反映されます。
この順序をモデルに OpenShift GitOps でアプリケーションを更新します。

### 3.1. アプリケーションソースコードの変更の適用
初めに、"Demo Health" アプリケーションのソースコードを変更し、"Demo Health" アプリケーションにログインした直後の画面に、地図を表示するウィンドウを追加します。

まず Gitea Console の上部のメニューから **"Expore" > "gitea / cicd-sample-app"** を選択し、**Branch: master** が選択されていることを確認します。
画面にディレクトリ/ファイル一覧が表示され、**"site" > "public" > "index.html"** と順番に辿り "index.html" ファイルの画面に移ります。
![Gitea CD App modify 01](./handson/img/gitea-cd-app-modify-01.png)

**"Edit file"** ボタンをクリックし、ファイルを編集します。
この画面で 73 行目から 78 行目に移動すると、これらの行がコメントアウトされていることがわかります。
このコメントを外すため、次の2行を行ごと削除します。

* 73行目 : `<!--`
* 78行目 : `-->`

削除した後は、下図のようになります。
![Gitea CD App modify 02](./handson/img/gitea-cd-app-modify-02.png)

変更内容に問題がなければ、**"Commit directly to the master branch."** のラジオボタンをチェックして、**"Commit Changes"** ボタンを押します。コミットメッセージを入力しても構いませんし、しなくても構いません。

**このハンズオンでは簡易的に手順を省略するため、直接 master ブランチにコミットしますが、実際の開発は master ブランチに直接コミットすることは通常せず、feature ブランチなど別のブランチにコミット後に master ブランチにマージリクエストして変更を反映してもらうことが一般的です。**

コミットしたら改めて "index.html" ファイルの中身を見て、変更が反映されていることを確認して下さい。

### 3.2. 開発環境へのアプリケーションの更新
開発環境の "Demo Health" アプリケーションのソースコードに変更を加えます。

アプリケーションのソースコード加えた変更をデプロイしたアプリケーションに反映させるには、コンテナイメージを再度ビルドし、ビルドしたイメージを使用するようにマニフェストリポジトリを更新します。
これは最初に作成した CI パイプラインを再び実行することで完了しますが、パイプラインは開発環境で実行させる必要があるため、`oc project "{USER}-develop"` で開発環境に移動することを忘れないよう注意して下さい。
```
cd ~/cd-practice/handson/pipelines
oc project ${USER}-develop
oc create -f handson-pipelinerun.yaml

[出力例]
pipelinerun.tekton.dev/handson-pipeline-run-xxxxx created
```

OpenShift Web コンソールでパイプラインの実行に成功したことを確認したら、OpenShift GitOps のダッシュボードから "{USER}-app-develop" Application を見ると、自動的に更新後のアプリケーションが反映されることがわかります。

これは、CI パイプラインによってマニフェストリポジトリが更新されたことで、Git リポジトリ上のマニフェストと OpenShift クラスタに登録されたリソースとの間にドリフトが発生し、Auto-sync により自動的にクラスタのリソースが修正されています。

それでは "{USER}-develop" Project の "Demo Health" アプリケーションに再びログインすると、先ほどまではなかった地図のウィンドウが右下に表示されているはずです。
![Argo CD Dev App modified 01](./handson/img/argocd-dev-app-modified-01.PNG)

### 3.3. ステージング環境へのアプリケーションの更新
開発環境で更新したアプリケーションをステージング環境に適用します。

まず "{USER}-staging" Project の "Demo Health" アプリケーションにログインし、右下に地図のウィンドウが無いことを確認して下さい。地図のウィンドウがあるのは、まだ開発環境だけです。

ステージング環境のアプリケーションを更新させる際、アプリケーションのソースコードを変更したり、OpenShift クラスタを操作したりすることはなく、Git リポジトリに "Pull Request" を送るだけです。

Git リポジトリに移り、画面上部の **"New Pull Request"** をクリックします。
![Argo CD Stg App modified 01](./handson/img/gitea-argocd-stg-app-modified-01.png)

"New Pull Request" の画面で、下図のように選択します。
* Target branch: merge into: gitea: **staging**
* Source branch: pull from: gitea: **master**
![Argo CD Stg App modified 02](./handson/img/gitea-argocd-stg-app-modified-02.png)

**"New Pull Request"** ボタンをクリックし、次の画面で特に入力せず **"Create Pull Request"** ボタンをクリックし、次のように **"Open"** という緑色のマークがついた画面に移ります。
これが先ほど作成した "プルリクエスト" の画面です。
ここでも何も触らずに、**"Merge Pull Request"** ボタンをクリックします。
![Argo CD Stg App modified 03](./handson/img/gitea-argocd-stg-app-modified-03.png)

すると、**"Merged"** という紫色のマークがついた画面に移ります。これはマージに成功したという画面です。
![Argo CD Stg App modified 04](./handson/img/gitea-argocd-stg-app-modified-04.png)

"マージ" はリポジトリのあるブランチに適用した更新内容を、別のブランチにも適用する処理です。
複数人でアプリケーションのリポジトリを共有して開発する場合、誰でも好き勝手にマージできると、コードは整合性を保つことが難しくなるため、一般的にマージさせる権限はチームリーダーや相応の役職を持つ開発者が該当します。
一方で権限を持たない人がマージを要求する場合、権限を持つ人にマージを依頼する、これが "マージリクエスト" です。
ここではマージリクエストもマージも自分自身で実行していますが、複数人で開発をすすめる場合、通常は行わないことを留意して下さい。

それでは "{USER}-staging" Project の "Demo Health" アプリケーションにログインし、変更が更新され、右下に地図が表示されることを確認します。
これは、前節の開発環境で実行した CI パイプラインにより **staging** ブランチに変更がマージされ、OpenShift GitOps がステージング環境のアプリケーションを更新したためです。

### 3.4. 本番環境へのアプリケーションの更新
ステージング環境に更新したアプリケーションを本番環境にも適用し、手順はステージング環境と同じです。
そのため本節は演習としたいと思います。詳細な手順は記載しませんが、ヒントを残しますので、挑戦してみて下さい。

本番環境でも "Demo Health" アプリケーションに地図が表示されるようになれば OK です。

**ヒント**

* Target branch: merge into: gitea: **production**
* Source branch: pull from: gitea: **staging**

**注意**
"Merge options" の "Delete source branch when merge request is accepted." のチェックは外して下さい。

### 3.5. アプリケーションのロールバック
大きなバグが見つかる、間違えたコードを適用するケースなど、アプリケーションに変更を加えた後、ロールバックにより切り戻しが必要な場合、OpenShift GitOps では簡単に戻すことができます。
全ての環境の "Demo Health" アプリケーションに地図が表示されましたが、機能的には問題ないもののメモリ消費が高いため、切り戻しが必要と判断したと想定します。
まず初めに切り戻すべきは本番環境ですが、前節までの手順で本番環境を更新するには 2 段階のマージリクエストが必要で手間がかかるので、OpenShift GitOps を用いて 1 段階の手順でロールバッにします。

OpenShift GitOps のダッシュボードから "{USER}-app-production" Application の画面を開きます。
![Argo CD Rollback 01](./handson/img/argocd-rollback-01.PNG)

画面上部の **"HISTORY AND ROLLBACK"** をクリックすると、下図のような画面が表示されます。
Application のデプロイの履歴を残していて、任意の状態まで戻すことができ、この画面では、一番上の Revision が最新、下の Revision ほど古いことを示します。
![Argo CD Rollback 02](./handson/img/argocd-rollback-02.PNG)

"{USER}-app-production" Application はまだ2回しかデプロイされていません。最新の Revision が地図付きのアプリケーションですので、1回目の Revision が地図なしのアプリケーションです。
画面で下の方の Revision の右側にある3つの点が並んだアイコンをクリックし、**"Rollback"** を選択します。
![Argo CD Rollback 03](./handson/img/argocd-rollback-03.PNG)

Rollback するためには Auto-sync を無効化しなくてはならないが本当に行うか、というポップアップが表示されますが、**"OK"** をクリックします。Auto-sync は後で有効化できます。
![Argo CD Rollback 04](./handson/img/argocd-rollback-04.PNG)

自動的に指定した Revision のアプリケーションに修正されます。
改めて "{GITLAB_USER}-production" Project の "Demo Health" アプリケーションを表示し、地図が取り外されているか確認します。
![Argo CD Rollback 05](./handson/img/argocd-rollback-05.PNG)

"OutOfSync" のステータスが表示されますが、これは Auto-sync が無効化されているためです。
ロールバックはあくまで以前のバージョンのマニフェストを使ってデプロイし直すだけであり、マニフェスト自体を以前のバージョンに戻すわけではありません。
そのため、ここですぐに Auto-sync を有効化してしまうと、また最新の地図付きのアプリケーションがデプロイされてしまいます。
改めて地図が無いソースコードに更新してパイプラインを回してマニフェストを更新するか、マニフェスト自体を元に戻す (`git revert`) か、どちらかによって地図が無い状態のマニフェストが最新になるまでは、Auto-sync は無効化しておきましょう。

以上でハンズオンは終了です。お疲れ様でした。

その他にサンプルやテスト用のアプリケーションをデプロイしたい方は次のリンク先を参照下さい。
* [Argo CD を使用した Spring Boot アプリケーションのデプロイ](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/cicd/index#deploying-a-spring-boot-application-with-argo-cd)
* [OpenShift GitOps - Enable your Developers to Deploy Applications Seamlessly​ Through ArgoCD](https://cloud.redhat.com/blog/openshift-gitops-enable-your-developers-to-deploy-applications-seamlessly-through-argocd)
