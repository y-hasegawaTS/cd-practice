# クラスタ管理者向け事前準備
このセクションはハンズオンのインストラクター向けにハンズオンの設定を準備するための手順です。
CI ハンズオンを既に同じクラスタで実施している場合、[事前準備](https://gitlab.com/openshift-starter-kit/ci-practice/-/tree/main/handson/preparations)のセクションで該当する準備は完了しているため、不要です。

ハンズオンでは、OpenShift で CI(継続的インテグレーション) と CD(継続的デリバリー) を実現するための標準機能である、OpenShift Pipelines と OpenShift GitOps を使用します。
* クラスタに OpenShift Pipelines をインストールする。
* クラスタに OpenShift GitOps をインストールする。
* ハンズオン受講者がクラスタにログインできるよう、IdP (Identity Provider) をセットアップする。
* ハンズオン受講者が Argo CD UI を操作できるよう、権限を変更する。

## OpenShift Pipelines のインストール
OpenShift Pipelines と OpenShift GitOps はそれぞれ Operator が用意されており、OpenShift の Web コンソールの OperatorHub から簡単にインストールすることができます。
まず Web コンソールに cluster-admin としてログインします。
![Console login with cluster-admin](../img/clusteradmin-login-01.PNG)
![Console login with cluster-admin](../img/clusteradmin-login-02.PNG)
![Console overview](../img/clusteradmin-login-03.PNG)

左のメニューから **"Operators" > "Installed Operators"** を選択すると、すでにインストールされているOperatorの一覧が表示されます。
![OperatorHub](../img/operatorhub-01.PNG)

**この一覧で OpenShift Pipelines が表示されている場合は、すでに OpenShift Pipelines はインストールされているため、本手順はスキップしてください。**

OpenShift Pipelines が表示されていないことを確認したら、左のメニューから **"Operators" > "OperatorHub"** を選択し、OperatorHub の画面に移ります。
OperatorHub の画面で、**"<em>Filter by keyword...</em>"** と表示されているテキストボックスに、**"openshift pipelines"** と入力すると、**"Red Hat OpenShift Pipelines"** の Operator が出てくるので、これをクリックします。
![OpenShift Pipelines Operator](../img/pipelines-install-01.PNG)

画面の右側に Operator の詳細が表示されます。上部にある **"Install"** のボタンをクリックします。
![OpenShift Pipelines Install](../img/pipelines-install-02.PNG)

OpenShift Pipelines Operator の channel や Installation mode などの設定画面が表示されます。設定は変更せず、デフォルトの設定のまま下部にある **"Install"** のボタンをクリックします。これで自動的にインストールが始まります。
![OpenShift Pipelines Subscibe](../img/pipelines-install-03.PNG)

再度左のメニューから **"Operators" > "Installed Operators"** を選択し、インストールされている Operator の一覧に、OpenShift Pipelines Operator が表示されていることを確認します。Operator の Status が **"Succeeded"** となるまで待ちます。  
(おおむね5分も待てばよいでしょう)

---
## OpenShift GitOps のインストール
OpenShift Pipelines のインストールと同様に、**"Operators" > "Installed Operators"** から OpenShift GitOps がインストールされていないことを確認します。

**この一覧で OpenShift GitOps が表示されている場合は、すでに OpenShift GitOps はインストールされているため、本手順はスキップしてください。**

OpenShift GitOps が表示されていないことを確認したら、左のメニューから **"Operators" > "OperatorHub"** を選択し、OperatorHub の画面に移ります。
OperatorHubの画面で、**"<em>Filter by keyword...</em>"** と表示されているテキストボックスに、**"openshift gitops"** と入力すると、**"Red Hat OpenShift GitOps"** の Operator が出てくるので、これをクリックします。
![OpenShift GitOps Operator](../img/gitops-install-01.PNG)

画面の右側に Operator の詳細が表示されます。上部にある **"Install"** のボタンをクリックします。
![OpenShift GitOps Install](../img/gitops-install-02.PNG)

OpenShift GitOps Operator の channel や Installation mode などの設定画面が表示されます。設定は変更せず、デフォルトの設定のまま下部にある **"Install"** のボタンをクリックします。これで自動的にインストールが始まります。
![OpenShift GitOps Subscibe](../img/gitops-install-03.PNG)

再度左のメニューから **"Operators" > "Installed Operators"** を選択し、インストールされている Operator の一覧に、OpenShift GitOps Operator が表示されていることを確認します。Operator の Status が **"Succeeded"** となるまで待ちます。  
(おおむね5分も待てばよいでしょう)

以下の図のように、OpenShift Pipelines と OpenShift GitOps の Operator がインストールされたことを確認できれば OK です。
![OpenShift Pipelines/GitOps Operator](../img/operatorhub-02.PNG)

## Identity Provider の設定
OpenShift Web Console や OpenShif CLI でクラスタにログインするためのユーザを作成します。
この手順は OpenShift クラスタに ROSA を利用し、ユーザアカウントに gitlab.com のアカウントを利用する想定です。

まず `rosa` コマンドを実行できる CLI ターミナルに入ります。
ここで、`rosa list cluster` を実行すると、稼働中の ROSA クラスタの名前が確認できます。  
この名前をオプションで指定して、`rosa create idp` コマンドを実行します。また、`--type` オプションには、`gitlab` を指定して実行しましょう。  
このコマンドは会話形式で進みます。初めに URL を尋ねられますが、ここでは "**https://gitlab.com**" と入力して Enter を押します。すると、下図の青字のような表示が返ってきます。
![ROSA IDP gitlab 01](../img/rosa-create-idp-gitlab-01.PNG)

この表示上の、次の 3行 は次の工程で使う情報なので重要です。

```
  - Name: rosa-xxxxx
  - Redirect URI: `https://oauth-openshift.apps.rosa-xxxxx.～
  - Scopes: openid
```

ここで、"**https://gitlab.com/-/profile/applications**" のリンク先に行きましょう。  
あなたの GitLab アカウントでサインインすると、下のような "Applications" という画面が表示されます。 
この画面で先に表示された 3行の情報を反映させます。
* "Name" のエディットボックスに、先のコマンドで表示されていた Name を入力
* "Redirect URI" のエディットボックスに、先のコマンドで表示されていた Redirect URI を入力
* Scopes セクションの "openid" のチェックボックスにチェック

![ROSA IDP gitlab 02](../img/rosa-create-idp-gitlab-02.PNG)

上図のようになったら、"Save application" のボタンをクリックします。
数秒後次のように Application の表示がされます。このまま開いておきましょう。

![ROSA IDP gitlab 03](../img/rosa-create-idp-gitlab-03.PNG)

再び CLI ターミナルに戻ります。Application ID を尋ねられているので、先の開いたままの画面から、"Application ID" をコピーして貼り付けて Enter を押します。  
次に Secret を尋ねられるので、また先の開いたままの画面から、"Secret" をコピーして貼り付けて Enter を押します。  
最後に Maaping method を尋ねられますが、"**claim**" を選択して Enter を押します。

そうすると、下図のように IdP がセットアップされた旨の表示が出てきます。

![ROSA IDP gitlab 04](../img/rosa-create-idp-gitlab-04.PNG)

2-3分ほど経ってから 一度 OpenShift Web コンソールからログアウトし、再びログイン画面を表示させてみましょう。  
次のように "gitlab-1" というボタンが表示されていれば OK です。
![Console login with gitlab](../img/user-login-01.PNG)

## Argo CD の権限設定
OpenShift GitOps (Argo CD) のユーザインタフェースからな操作する際、一般ユーザは read-only が許可され admin 権限がないため、Gitlab リポジトリを登録するなどの操作ができません。  
そのため Argo CD ユーザインタフェースで admin として操作するための権限を付与します。

CLI ターミナルを表示して次のコマンドを実行します。
```
oc create clusterrolebinding appprojects-edit --clusterrole=appprojects.argoproj.io-v1alpha1-edit --group=system:authenticated
oc create clusterrolebinding applications-edit --clusterrole=applications.argoproj.io-v1alpha1-edit --group=system:authenticated
oc patch argocd openshift-gitops -n openshift-gitops --type='json' -p='[{"op": "replace", "path": "/spec/rbac/policy", "value":"g, system:authenticated, role:admin\n"}]'
```
---

セットアップ作業は以上です。